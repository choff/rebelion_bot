// libreria que maneja los feeds
const Parser = require('rss-parser');
const parser = new Parser();
const config = require('../config');

// retorna objecto con los feeds de cada fuente
async function getFeeds(url_feeds) {
  let newFeeds = {};
  let feeds = Object.keys(url_feeds);

  // itera sobre los feeds
  for(let i = 0; i < feeds.length; i++) {
    let source = feeds[i];
    let feed = await parser.parseURL(url_feeds[source]);
    feed.items.forEach((item, idx) => {
      if(!newFeeds.hasOwnProperty(source))
        newFeeds[source] = {};
      newFeeds[source][item.guid] = item;
    });
  }
  return newFeeds;
}

// devuelve dos arreglos con los guid a borrar y a agregar
function getChanges(source, newFeeds, DIM) {
  let oldest_guid = Object.keys(DIM[source]);
  let newest_guid = Object.keys(newFeeds[source]);
  let to_delete = [];
  let to_add = [];
  
  // obtiene el guid a borrar
  oldest_guid.forEach(old_guid => {
    // si old_guid no esta en newFeeds
    if(newest_guid.indexOf(old_guid) == -1) 
      to_delete.push(old_guid);
  });

  // obtiene el guid a agregar
  newest_guid.forEach(new_guid => {
    if(oldest_guid.indexOf(new_guid) == -1) 
      to_add.push(new_guid);
  });

  return { to_delete, to_add };
}

// update data in memory
function updateDIM (to_delete, to_add, source, newFeeds, DIM) {
  // delete from DIM
  to_delete.forEach( guid => delete DIM[source][guid]);

  // add to DIM
  to_add.forEach( guid => DIM[source][guid] = {...newFeeds[source][guid]});
}

// actualiza los Feeds en memoria y retorna un objecto con los
// nuevos feeds a enviar
async function updateFeedsDIM(DIM, url_feeds) {
  let newFeeds = await getFeeds(url_feeds);
  let feedsToSend = {};
  
  Object.keys(newFeeds).forEach(source => {
    let { to_delete, to_add } = getChanges(source, newFeeds, DIM); 
    updateDIM(to_delete, to_add, source, newFeeds, DIM);

    feedsToSend[source] = {};
    to_add.forEach(guid => feedsToSend[source][guid] = newFeeds[source][guid]);
  });

  return feedsToSend;
}

module.exports = {
  updateFeedsDIM
};
