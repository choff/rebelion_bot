
// bot telegram
const Telegraf = require('telegraf');
const token = '739214167:AAFUqckyLcO2PCltWuePANCRtCVDjHMw4Es';
const bot = new Telegraf(token);

// feeds handler
const Feed = require('./lib/feeds'),
      User = require('./lib/user'),
      config = require('./config');

const url_feeds = config.feeds,
      interval = 1000 * 60 * config.minutes; 

// bandera que previene el envio de mensajes en la primera ejecucion
let init_flag = 1;

// datos en memoria de los ultimos feeds
let DIM = {};
Object.keys(url_feeds).forEach(source => DIM[source] = {});


// id de usuarios
let usersId;

// carga id de usuarios desde archivo
User.readUsers()
  .then(data => {
    console.log('users from file:', data);
    usersId = [...data];
  });

// maneja los errores del bot
bot.catch((err) => {
  console.log('Demonios!!! ', err);
});

// test 
bot.command('ping', (ctx) => ctx.reply('Estoy vivo !!'));

bot.start(async (ctx) => {
  let user_id = ctx.message.chat.id;
  // guarda el id del usuario
  if (usersId.indexOf(user_id) == -1) {
    usersId.push(user_id);
  }
  await User.saveUsers(usersId);
  ctx.reply('Bienvenid@');
});

// broadcast message
const sendMsg = function(Msg){
  usersId.forEach((id) => bot.telegram.sendMessage(id,Msg));
};

// actualiza los feed y obtiene un objeto con los feeds a enviar
async function updateFeeds(DIM, url_feeds, init_flag) {
  let feedsToSend = await Feed.updateFeedsDIM(DIM, url_feeds);
  if(!init_flag) {
    Object.keys(feedsToSend).forEach(source => {
      //console.log(source);
      Object.keys(feedsToSend[source]).forEach(guid => {
        //console.log(feedsToSend[source][guid]);
        sendMsg(guid);
      });
    });
  }
  init_flag = 0;
}

// refresh news
setInterval(updateFeeds, interval, DIM, url_feeds, init_flag);
//setInterval(sendMsg, 60*1000, 'hola Mundo bot');
bot.launch();


