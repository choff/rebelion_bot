// config for bot

// list of feeds to handle
let feeds = {
  rebelion: 'http://www.rebelion.org/rss_portada.php'
//  prensaRural: 'https://www.prensarural.org/spip/spip.php?page=backend'
};

// interval in minutes to refresh info
let minutes = 60;

// users file
let userFile = __dirname + '/users';


module.exports = {
  feeds,
  minutes,
  userFile
};
